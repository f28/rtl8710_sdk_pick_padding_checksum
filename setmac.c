/*
 * setmac.c
 * compilation: gcc setmac.c -o setmac -s
 *
 *  Created on: Aug 24, 2017
 *      Author: sharikov
 *
 */


#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdint.h>

#define MAC2STR(a) (a)[0], (a)[1], (a)[2], (a)[3], (a)[4], (a)[5]
#define MACSTR "%02x:%02x:%02x:%02x:%02x:%02x"

#define MAC_REALMAP_OFFSET 0x11a
// смещение в _ADAPTER.eeprompriv.efuse_eeprom_data[512]
// в ameba это ST_MAC. AP_MAC=ST_MAC+1
#define MAC_SIZE 6

#define MAGIC_HEADER 0x8195
uint8_t  eeprom_data[4096];
uint16_t eeprom_data_configured[4096];

/* читает 1 запись config
 * на выходе:
 *	-1: конец по data_addr
 *	-2: конец по data_len
 *	-3: конец по data_addr + data_len
 */
int read_one_record(FILE* ifp) {
	int found=0;
	int i;
	uint16_t data_addr, data_len;

	//прочитаем заголовок
	fread((void*)&data_addr, 1, 2, ifp);
	if (data_addr == 0xffff)
		return -1;
	fread((void*)&data_len, 1, 2, ifp);
	if (data_len == 0xffff)
		return -2;
	if ( (data_addr + data_len) > 4096 )
		return -3;

	//прочитаем данные
	fread((void*)&eeprom_data[data_addr], data_len, 1, ifp);

	// пометим что данные конфигурации обновлены
	for (i=0; i<data_len; i++) {
		int idx = data_addr+i;
		eeprom_data_configured[idx]++;
	}
	return found;
}
// читаем всю конфигурацию
int import_calibration(FILE* ifp) {
	int rcount=0;

	do {
		if (read_one_record(ifp) <0)
			break;
		rcount++;
	}
	while (rcount < 1024);
	return rcount;
}

int write_one_record(FILE* ofp, uint16_t data_addr, uint16_t data_len) {
	//printf("addr=0x%x:\tlen=0x%x\n", data_addr, data_len );
	fwrite((void*)&data_addr, 2, 1, ofp);
	fwrite((void*)&data_len,  2, 1, ofp);
	return fwrite((void*)&eeprom_data[data_addr],  1, data_len, ofp);
}

int export_calibration(FILE* ofp) {
	int rcnt=0;
	uint16_t data_addr, data_addr_wr;
	uint16_t data_len_wr=0;
	uint8_t wr=0;

	for (data_addr=0; data_addr<sizeof(eeprom_data); data_addr++) {
		if (wr !=0) {
			// сохраняем если встретилось не сконфигурированное значение или
			// при переходе границы 16 байт
			if ((eeprom_data_configured[data_addr] == 0) ||
					((data_addr & 0xf0) != (data_addr_wr & 0xf0)) ) {
				// записывыаем на диск
				if (write_one_record(ofp, data_addr_wr, data_len_wr) != data_len_wr)
					return -1;
				wr = 0;
			}
		}
		if ((eeprom_data_configured[data_addr] != 0)) {
			// начало записи ?
			if (wr==0) {
				data_addr_wr = data_addr;   // сохраним начальный адрес записи
				wr =1;           // надо записать
				data_len_wr=0;
			}
			data_len_wr++;
		}
	}
	// если wr!=0 значит последняя итерация не записана
	if (wr !=0) {
		if (write_one_record(ofp, data_addr_wr, data_len_wr) != data_len_wr)
			return -1;
	}
	return 0;
}

/*
 * стандартный формат: "XX:XX:XX:XX:XX:XX"
 * формат OUI+номер  : "XX:XX:XX[:XX]+nnn"
 */
int decode_macstr_from_user(char* macstr, char* newmac) {
	int cc;
	char *numptr;
	int res;
	int val[MAC_SIZE] = { -1, -1, -1, -1, -1, -1 };

	numptr = strchr(macstr, '+');
	if (numptr) {
		// OUI+номер
		res = sscanf(macstr, "%02x:%02x:%02x+%d", &val[0], &val[1], &val[2], &val[3]);
		if (res <0) {
			printf("Invalid MAC string %s\n", macstr);
			return -1;
		}
		// проверим номер
		if ((val[3] <0) || (val[3] > 0xffffff)) {
			printf("MAC (OUI+num format) error: invalid number %d\n", val[3]);
			return -1;
		}
		// копируем OUI
		for (cc=0; cc<3; cc++)
			newmac[cc] = val[cc];
		// добавляем номер
		newmac[3] = (val[3] >> 16) & 255;
		newmac[4] = (val[3] >>  8) & 255;
		newmac[5] =  val[3]        & 255;
	}
	else {
		// стандартный формат
		res = sscanf(macstr, MACSTR,
				&val[0], &val[1], &val[2], &val[3], &val[4], &val[5]);
		if (res != MAC_SIZE) {
			printf("Invalid MAC (standart format)\n");
			return -1;
		}

		// формируем MAC
		for (cc=0; cc < MAC_SIZE; cc++)
			newmac[cc] = val[cc];
	}
	// в Ameba младший байт MAC не может быть равен ff
	// произойдет переполнение AP_MAC
	// ((char)AP_MAC[5]=(char)ST_MAC[5]+1 --> 0xff+1 = 0)
	if (newmac[5] == (char)255) {
		printf("Bad MAC! MAC[5] = 0xff\n");
		return -1;
	}

	// проверим сформированный MAC на 0 и ff
	res=0;
	for (cc=0; cc < MAC_SIZE; cc++) {
		res |= newmac[cc];
	}
	if (res==0) {
		printf ("Error! MAC = 00:00:00:00:00:00 \n");
		return -1;
	}
	res=0xff;
	for (cc=0; cc < MAC_SIZE; cc++) {
		res &= newmac[cc];
	}
	if (res==0xff) {
		printf ("Error! MAC = FF:FF:FF:FF:FF:FF \n");
		return -1;
	}

	return 0;
}

void print_usage() {
	printf("Usage: setmac -i oldcalibration [-o updatedcalibration] [-v] [-n new_mac]\n");
	printf(" oldcalibration - current calibration data (binary)\n");
	printf(" updatedcalibration - updated calibration data (binary)\n");
	printf(" new_mac - new MAC\n");
	printf(" supported MAC format:\n");
	printf("  standart  : XX:XX:XX:XX:XX:XX\n");
	printf("  OUI+serial: XX:XX:XX[:XX:XX:XX]+nnn\n");
}

int main(int argc, char *argv[])
{
	char* ifn = NULL;
	char* ofn=NULL;
	char* macstr=NULL;
	char  newmac[6];
	FILE *ifp = NULL;
	FILE *ofp = NULL;
	int  retcode = EXIT_SUCCESS;
	uint16_t val16;
	int rcount=0;
	int option = 0;

	while ((option = getopt(argc, argv,"i:o:m:")) != -1) {
		switch (option) {
		case 'i' :
			ifn = optarg;
			break;
		case 'o' :
			ofn = optarg;
			break;
		case 'm' :
			macstr=optarg;
			break;
		default:
			print_usage();
			exit(EXIT_FAILURE);
		}
	}
	if (ifn==NULL) {
		print_usage();
		exit(EXIT_FAILURE);
	}
	if (ofn && macstr==NULL) {
		print_usage();
		exit(EXIT_FAILURE);
	}

	if (macstr) {
		if (decode_macstr_from_user(macstr, newmac) <0)
			exit(EXIT_FAILURE);

	}

	memset(eeprom_data_configured, 0, sizeof(eeprom_data_configured));
	memset(eeprom_data, 0xff, sizeof(eeprom_data));


	ifp = fopen(ifn, "rb+");
	if (!ifp) {
		retcode = 2;
		goto exit_lbl;
	}
	if (ofn) {
		ofp = fopen(ofn, "wb+");
		if (!ofp) {
			retcode = 2;
			goto exit_lbl;
		}
	}

	// проверим заголовок файла
	fread((void*)&val16, 1, 2, ifp);
	if (val16 != MAGIC_HEADER) {
		printf("Magic Header mismatch! (0x%x)\n", val16);
		retcode = -1;
		goto exit_lbl;
	}

	rcount = import_calibration(ifp);

	if (rcount ==0) {
		retcode = EXIT_FAILURE;
		printf("No calibration data\n");
		goto exit_lbl;
	}

	else {
		if (	(eeprom_data_configured[MAC_REALMAP_OFFSET+0] !=0) ||
				(eeprom_data_configured[MAC_REALMAP_OFFSET+1] !=0) ||
				(eeprom_data_configured[MAC_REALMAP_OFFSET+2] !=0) ||
				(eeprom_data_configured[MAC_REALMAP_OFFSET+3] !=0) ||
				(eeprom_data_configured[MAC_REALMAP_OFFSET+4] !=0) ||
				(eeprom_data_configured[MAC_REALMAP_OFFSET+5] !=0) ) {
			printf("oldMAC=%02x:%02x:%02x:%02x:%02x:%02x\n",
					MAC2STR(&eeprom_data[MAC_REALMAP_OFFSET]));
		}
		else {
			printf("MAC not found\n");
			retcode = EXIT_FAILURE;
			goto exit_lbl;
		}
	}

	if (ofp) {
		//  вставим новый MAC
		for (val16=0; val16 < MAC_SIZE; val16++) {
			eeprom_data[MAC_REALMAP_OFFSET + val16]=newmac[val16];
			eeprom_data_configured[MAC_REALMAP_OFFSET+val16]++;
		}
		printf("newMAC=%02x:%02x:%02x:%02x:%02x:%02x\n",
				MAC2STR(&eeprom_data[MAC_REALMAP_OFFSET]));
		// создадим новый калибровочный файл
		val16 = MAGIC_HEADER;
		if (fwrite((void*)&val16, 2, 1, ofp)!=1) {
			retcode = 2;
			goto exit_lbl;
		}

		if (export_calibration(ofp) !=0) {
			printf("Write error!\n");
			retcode = 2;
		}
	}


	exit_lbl:
	if (ifp) {
		fclose(ifp);
		ifp=NULL;
	}
	if (ofp) {
		fclose(ofp);
		ofp=NULL;
	}
	return retcode;
}
